package com.example.fizzbuzz;

import org.junit.jupiter.api.Test;

import java.util.List;

import static com.example.fizzbuzz.FizzBuzzCalculator.calculateFizzBuzz;
import static org.assertj.core.api.Assertions.assertThat;

class FizzBuzzCalculatorUnitTest {
    @Test
    public void testFizzBuzz() {
        List<String> values = calculateFizzBuzz();
        assertThat(values).isNotNull().hasSize(100);
        assertThat(values.get(0)).isNotNull().isEqualTo("1");
        assertThat(values.get(2)).isNotNull().isEqualTo("Fizz");
        assertThat(values.get(4)).isNotNull().isEqualTo("Buzz");
        assertThat(values.get(14)).isNotNull().isEqualTo("Fizzbuzz");
    }
}