package com.example.fizzbuzz;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class FizzbuzzApplication {
    public static void main(String[] args) {
        SpringApplication.run(FizzbuzzApplication.class, args);
    }
}
