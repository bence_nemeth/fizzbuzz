package com.example.fizzbuzz;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FizzBuzzCalculator {
    public static final int RANGE_START = 1;
    public static final int RANGE_END = 100;
    public static final String FIZZ = "Fizz";
    public static final String BUZZ = "Buzz";
    public static final String FIZZBUZZ = "Fizzbuzz";

    public static List<String> calculateFizzBuzz() {
        return IntStream.rangeClosed(RANGE_START, RANGE_END)
                .mapToObj(i -> i % 3 == 0 ? (i % 5 == 0 ? FIZZBUZZ : FIZZ) : (i % 5 == 0 ? BUZZ : i))
                .map(Objects::toString)
                .collect(Collectors.toList());
    }
}
