package com.example.fizzbuzz;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.example.fizzbuzz.FizzBuzzCalculator.calculateFizzBuzz;

@RestController
@Slf4j
public class Controller {
    @GetMapping("/fizzbuzz")
    public String fizzbuzz() {
        return calculateFizzBuzz().toString();
    }
}
