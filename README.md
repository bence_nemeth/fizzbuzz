## Basic fizzbuzz application.

After starting the program and calling localhost:8080/fizzbuzz with basic authentication (user/pass), it will return the numbers from 1 to 100 (Fizz on numbers divisible with 3, Buzz on 5, Fizzbuzz on 3 and 5).
